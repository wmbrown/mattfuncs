# varsearch searches the variable names of a dataset (dataframe or datatable)
# ds is the dataset to to be checked
# chars is the string to search for. omitting this will display all the column names of the dataset
vs <- function(chars="", ds, ignore_case = TRUE) {
  return(colnames(ds)[grepl(chars,colnames(ds), ignore.case = ignore_case)])
}

